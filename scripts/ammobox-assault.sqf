while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 100;
_itemAmount = 15;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;


_this additemcargoGlobal ["optic_mas_zeiss", _itemAmount];
_this additemcargoGlobal ["optic_mas_zeiss_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_zeiss_eo", _itemAmount];
_this additemcargoGlobal ["optic_mas_zeiss_eo_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_eo", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_eo_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_rd", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_rd_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_aim_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_DMS", _itemAmount];
_this additemcargoGlobal ["optic_mas_DMS_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_Holosight_blk", _itemAmount];
_this additemcargoGlobal ["optic_mas_Holosight_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_Arco_blk", _itemAmount];
_this additemcargoGlobal ["optic_mas_Arco_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_Hamr_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_Aco_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_ACO_grn_camo", _itemAmount];
_this additemcargoGlobal ["optic_mas_MRCO_camo", _itemAmount];


//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];
_this addweaponcargoGlobal ["arifle_mas_m4", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_m4_m203_d", _itemAmount];
_this addweaponcargoGlobal ["arifle_mas_m16_gl", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_g3", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_fal", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_m1014", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_mp5_d", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_saiga", _weaponAmount];




_this addmagazinecargoGlobal ["30Rnd_556x45_Stanag", _magAmount];
_this addmagazinecargoGlobal ["20Rnd_mas_762x51_Stanag", _magAmount];
_this addmagazinecargoGlobal ["7Rnd_mas_12Gauge_Slug", _magAmount];
_this addmagazinecargoGlobal ["7Rnd_mas_12Gauge_Pellets", _magAmount];
_this addmagazinecargoGlobal ["30Rnd_mas_9x21d_Stanag", _magAmount];
_this addmagazinecargoGlobal ["10Rnd_mas_12Gauge_Slug", _magAmount];
_this addmagazinecargoGlobal ["10Rnd_mas_12Gauge_Pellets", _magAmount];

_this addmagazinecargoGlobal ["HandGrenade", _magAmount];
_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["3Rnd_Smoke_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["3Rnd_SmokeBlue_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["UGL_FlareRed_F", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_HE_Grenade_shell", _magAmount];

sleep _sleepAmount;

};