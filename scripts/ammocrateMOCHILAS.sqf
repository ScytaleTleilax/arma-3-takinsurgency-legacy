while {alive _this} do{

//Settings 

_itemAmount = 24;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;

//Chest

//_this addItemCargoGlobal ["V_HarnessO_brn", _itemAmount];
//_this addItemCargoGlobal ["V_HarnessOGL_brn", _itemAmount];
//_this addItemCargoGlobal ["V_PlateCarrier1_cbr", _itemAmount];
_this addItemCargoGlobal ["V_PlateCarrier1_rgr", _itemAmount];
_this addItemCargoGlobal ["V_TacVest_brn", _itemAmount];
//_this addItemCargoGlobal ["V_TacVest_khk", _itemAmount];
//_this addItemCargoGlobal ["V_TacVest_oli", _itemAmount];

//Backpacks:
//_this addBackpackCargoGlobal ["tf_rt1523g", 3];
_this addBackpackCargoGlobal ["B_AssaultPack_Base", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_blk", _itemAmount];
_this addBackpackCargoGlobal ["B_AssaultPack_cbr", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_dgtl", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_khk", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_mcamo", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_ocamo", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_rgr", _itemAmount];
//_this addBackpackCargoGlobal ["B_AssaultPack_sgg", _itemAmount];
//_this addBackpackCargoGlobal ["B_Bergen_Base", _itemAmount];
//_this addBackpackCargoGlobal ["B_Bergen_sgg", _itemAmount];
_this addBackpackCargoGlobal ["B_Carryall_Base", _itemAmount];
//_this addBackpackCargoGlobal ["B_Carryall_ocamo", _itemAmount];
//_this addBackpackCargoGlobal ["B_Carryall_oucamo", _itemAmount];
_this addBackpackCargoGlobal ["B_Kitbag_Base", _itemAmount];
//_this addBackpackCargoGlobal ["B_Kitbag_cbr", _itemAmount];
//_this addBackpackCargoGlobal ["B_Kitbag_mcamo", _itemAmount];
//_this addBackpackCargoGlobal ["B_Kitbag_sgg", _itemAmount];
//_this addBackpackCargoGlobal ["Bag_Base", _itemAmount];

sleep _sleepAmount;

};