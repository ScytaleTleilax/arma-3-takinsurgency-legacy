//Zum Beenden der Mission �ber 'ende1', wenn die als Parameter
//  �bergebene Anzahl an Zivilisten get�tet wurde.
//
//Aufrufbeispiel f�r 5 tote Zivilisten:
//  5 execVM "maxCivKills.sqf"
//
//Die Zivilisten k�nnen vor oder w�hrend des Spielens erstellt werden.
 
private = ["_tote","_CivsNeu","_CivsAlt"];

_tote    = 0; //Z�hler f�r tote Zivilisten
_CivsAlt = 0;      
while {_tote < _this} do //Schleife l�uft solange Totengrenze nicht erreicht
  {
    waitUntil {civilian countside allUnits != _CivsAlt};
    _CivsNeu = civilian countside allUnits; //aktuell vorhandene Zivilisten
    if (_CivsNeu < _CivsAlt) then
      {
        _tote = _tote + (_CivsAlt - _CivsNeu) 
      };
    _CivsAlt = _CivsNeu
  };
ende1 = true;
publicvariable "ende1";

//Nur zum Testen:
player groupChat format["Tote Zivilisten: %1  Spielende ende1 erreicht!",_tote];