//Set the level of the AI
//east = OPFOR
//independiente = Independiente

sleep 60;
while {true} do {
	_enemyCount = 0;
	_civilCount = 0;
	_iaCount=0;
	{
		if ((side _x == east) OR (side _x == independent)) then {
		
			_x setUnitAbility 0.2;
			_x setskill ["aimingAccuracy",0.3];
			_x setskill ["aimingShake",0.5];
			_x setskill ["aimingSpeed",0.3];
			_x setskill ["Endurance",0.4];
			_x setskill ["spotDistance",1.0];
			_x setskill ["spotTime",1.0];
			_x setskill ["courage",0.8];
			_x setskill ["reloadSpeed",0.4];		
			
			_x addEventHandler ["HandleDamage",{_damage = 1.5;_damage}];
			
			_enemyCount = _enemyCount + 1;
		};
		
		if (side _x == civilian) then {
			_civilCount = _civilCount + 1;
			//_x setBehaviour "SAFE";
		};		
		
		_iaCount = _enemyCount + _civilCount;
				
	} forEach allUnits;

	diag_log format["RENDIMIENTO: Time: %1, fps: %2, fpsmin: %3, IACount: %4", time, diag_fps, diag_fpsmin, _iaCount];
	
	sleep 120;
};